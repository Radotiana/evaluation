<?php
defined('BASEPATH') or exit('No direct script access allowed');
class TarifController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("TarifModel");
        $this->load->model("TrajetModel");
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->library('pagination');
    }
    public function index()
    {
        // $start = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        // $limit=5;
        $data['tarif']=$this->TarifModel->listTarifs();

        // $config['base_url'] = base_url('TarifController?');
        // $config['total_rows'] = count($this->TarifModel->listTarifs());
        // $config['per_page'] = $limit;
        // $config["uri_segment"] = 3;

        // $this->pagination->initialize($config);

        // $data['links']=$this->pagination->create_links();
        
        $data['error']="";
        $nb=$this->input->get('nb');
        $this->load->view('listTarif', $data);
    }
    public function modifierTarif()
    {
        $data['error']="";
        $idTarif=$this->input->get('idTarif');
        $tarifs=$this->TarifModel->listTarifById($idTarif);
        $data['tarif']=$tarifs;
        $data['trajet']=$this->TrajetModel->listTrajets();        
        $this->load->view('updateTarif',$data);
    }
    public function supprimerTarif()
    {
        $data['error']="";
        $idTarif=$this->input->get('idTarif');
        $this->TarifModel->deleteTarif($idTarif);
        redirect('TarifController/');
    }
    public function updateTarif()
    {
        $idTarif=$this->input->get('idTarif');
        $data["error"]="";
        try {
            $trajet=$this->input->get('trajet');
            $prix=$this->input->get('prix');
            $datas["IDTRAJET"]=$trajet;
            $datas["PRIX"]=$prix;
            if($prix<=0)
            {
                throw new Exception("Le prix ne doit pas être inferieur ou égal à 0");
            }
            $this->TarifModel->updateTarif($idTarif, $datas);
            redirect('TarifController/');   
        } catch (Exception $e) {
            $data['error']=$e->getMessage();
            $data['tarif']=$this->TarifModel->listTarifs();
            $this->load->view('listTarif', $data);
        }
    }
    public function ajoutTarif()
    {
        $data['error']="";
        $data['trajet']=$this->TrajetModel->listTrajets();
        $this->load->view('addTarif', $data);
    }
    public function addTarif()
    {
        $data["error"]="";
        try {
            $trajet=$this->input->get('trajet');
            $prix=$this->input->get('prix');
            $datas["IDTRAJET"]=$trajet;
            $datas["PRIX"]=$prix;
            $tarifs=$this->TarifModel->listTarifByIdTrajet($trajet);
            if(count($tarifs)!=0)
            {
                throw new Exception("Il y a deja une tarif qui correspond à ce reference de trajet");
            }
            if($prix<=0)
            {
                throw new Exception("Le prix ne doit pas être inferieur ou égal à 0");
            }
            
            // echo $date." - ".$heure;
            $this->TarifModel->addTarif($datas);
            redirect('TarifController/');
        } catch (Exception $e) {
            $data['error']=$e->getMessage();
            $data['trajet']=$this->TrajetModel->listTrajets();
            $this->load->view('addTarif', $data);
        }
    }
    public function search()
    {
        $mot=$this->input->get('search');
        $data['error']="";
        $data['tarif']=$this->TarifModel->search($mot);
        $this->load->view('listTarif', $data);
    }
}