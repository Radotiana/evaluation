<?php
defined('BASEPATH') or exit('No direct script access allowed');
class TrajetController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("TrajetModel");
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
    }
    public function index()
    {
        $data['error']="";
        $data['trajet']=$this->TrajetModel->listTrajets();
        $this->load->view('listTrajet', $data);
    }
    public function modifierTrajet()
    {
        $data['error']="";
        $idTrajet=$this->input->get('idTrajet');
        $trajets=$this->TrajetModel->listTrajetById($idTrajet);
        $data['trajet']=$trajets;
        $data['ville']=$this->TrajetModel->getVille();
        $this->load->view('updateTrajet',$data);
    }
    public function supprimerTrajet()
    {
        $data['error']="";
        $idTrajet=$this->input->get('idTrajet');
        $this->TrajetModel->deleteTrajet($idTrajet);
        redirect('TrajetController/');
    }
    public function updateTrajet()
    {
        $idTrajet=$this->input->get('idTrajet');
        $data["error"]="";
        try {
            $depart=$this->input->get('depart');
            $arrive=$this->input->get('arrive');
            $distance=$this->input->get('distance');
            $datas["DEPART"]=$depart;
            $datas["ARRIVE"]=$arrive;
            $datas["DISTANCE"]=$distance;
            if($distance<=0)
            {
                throw new Exception("La distance ne doit pas être inférieur ou égal à 0");
            }
            if($depart==$arrive)
            {
                throw new Exception("La depart ne doit pas être aussi l'arrivée");
            }
            $this->TrajetModel->updateTrajet($idTrajet, $datas);
            redirect('TrajetController/');            
        } catch (Exception $e) {
            $data['error']=$e->getMessage();
            $data['trajet']=$this->TrajetModel->listTrajets();
            $this->load->view('listTrajet', $data);
        }
    }
    public function ajoutTrajet()
    {
        $data['error']="";
        $data['ville']=$this->TrajetModel->getVille();
        $this->load->view('addTrajet', $data);
    }
    public function addTrajet()
    {
        $data["error"]="";
        try {
            $depart=$this->input->get('depart');
            $arrive=$this->input->get('arrive');
            $distance=$this->input->get('distance');
            $datas["DEPART"]=$depart;
            $datas["ARRIVE"]=$arrive;
            $datas["DISTANCE"]=$distance;
            if($distance<=0)
            {
                throw new Exception("La distance ne doit pas être inférieur ou égal à 0");
            }
            if($depart==$arrive)
            {
                throw new Exception("La depart ne doit pas être aussi l'arrivée");
            }
            $list=$this->TrajetModel->listTrajetDepartArrive($depart,$arrive);
            if(count($list)!=0)
            {
                throw new Exception("Il y a deja un trajet qui correspond à cette depart et arrivée");
            }
            $this->TrajetModel->addTrajet($datas);
            redirect('TrajetController/');
        } catch (Exception $e) {
            $data['error']=$e->getMessage();
            $this->load->view('addTrajet', $data);
        }
    }
    public function search()
    {
        $mot=$this->input->get('search');
        $data['error']="";
        $data['trajet']=$this->TrajetModel->search($mot);
        $this->load->view('listTrajet', $data);
    }
}