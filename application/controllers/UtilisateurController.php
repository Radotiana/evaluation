<?php
defined('BASEPATH') or exit('No direct script access allowed');
class UtilisateurController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("UtilisateurModel");
        $this->load->model("VolModel");
        // $this->load->model("model_user");
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
    }
    public function listPrevision()
    {
        $data['error']="";
        $data['vols']=$this->VolModel->listVols();
        $data['liste']=$this->UtilisateurModel->listVolPrevisionnelle(1);
        $this->load->view('listPrevision',$data);
    }
    public function valider()
    {
        $idAchats=$this->input->get('validation');
        $idVol=$this->input->get('idVol');
        for($i=0; $i<count($idAchats); $i++)
        {
            $this->UtilisateurModel->updateEtatAchat($idAchats[$i]);
        }
        $this->UtilisateurModel->updateVol($idVol);
        redirect('UtilisateurController/listPrevision');
    }
    public function search()
    {
        $data['error']="";
        $idVol=$this->input->get('vol');
        $data['vols']=$this->VolModel->listVols();
        $data['liste']=$this->UtilisateurModel->listVolPrevisionnelle($idVol);
        $this->load->view('listPrevision',$data);
    }
    public function chiffre()
    {
        $data['montant']=0;
        $data['daty']=null;
        $data['vols']=$this->VolModel->listVolParti();
        $idVol=$this->input->get('vol');
        // var_dump($idVol);
        if($idVol==null)
        {
            $idVol=0;
        }
        $chiffres=$this->VolModel->getChiffre($idVol);
        // var_dump($chiffres);
        $montant=0;
        $chiffreDaf=$this->VolModel->getchiffres($idVol);
        $d=[];
        if(count($chiffreDaf)!=0)
        {
            $d["idVol"]=$chiffreDaf[0]->IDVOL;
            $d["montant"]=$chiffreDaf[0]->MONTANT;
        }
        for($i=0; $i<count($chiffres); $i++)
        {
            if($chiffres[$i]->CONDITIONS==5 || $chiffres[$i]->CONDITIONS==2)
            {
                $data['montant']=$montant+$chiffres[$i]->MONTANT;
                $data['daty']=$chiffres[$i]->DATY;
                $mon=$data['montant'];
            }
            else if($chiffres[$i]->CONDITIONS==1 || $chiffres[$i]->CONDITIONS==0)
            {
                //ATAO NULL NY DATE DEPART ANLE OLONA
                $this->VolModel->updateModif($chiffres[$i]->IDACHAT);
            }
            
            // if(count($chiffreDaf)==0)
            if($chiffres[$i]->CONDITIONS==5 || $chiffres[$i]->CONDITIONS==2)
            {
                $chiffres=$this->VolModel->getChiffres($idVol);
                if(count($chiffres)==0)
                {
                    $dat["IDVOL"]=$idVol;
                    $dat["MONTANT"]=$mon;
                    // var_dump($dat);
                    $this->VolModel->addChiffre($dat);
                }
            }
        }
        $data["nouveau"]=$d;
        $this->load->view('chiffre',$data);
    }
    public function acheter()
    {
        try
        {
            $nom=$this->input->get('nom');
            $prenom=$this->input->get('prenom');
            $utilisateur=$this->session->userdata('Utilisateur')->IDUTILISATEUR;
            $vol=$this->input->get('vol');
            $tarif=$this->VolModel->getTarifByVol($vol)[0]->PRIX;
            $capacite=$this->VolModel->getAvionByVol($vol)[0]->NBPLACE;
            $count=$this->VolModel->getAchatTotal($vol)[0]->NB;
            // var_dump($count);
            // echo "count est ".$count. "et capacite est ".$capacite;
            if($count>$capacite)
            {
                throw new Exception("L'avion est deja plein");
            }
            // echo $tarif;
            $prix=100;
            $option=$this->input->get('option');
            if($option==1)
            {
                $prix=50;
            }
            elseif($option==2)
            {
                $prix=10;
            }
            // echo $prix;
            $reduction=0;
            $condition=10;

            if($this->input->get('modifiable')!=null && $this->input->get('remboursable')==null)
            {
                $condition=1;
                $reduction=$reduction+15;
            }
            if($this->input->get('remboursable')!=null && $this->input->get('modifiable')==null) 
            {
                $condition=2;
                $reduction=$reduction+15;
            }
            if($this->input->get('modifiable')!=null && $this->input->get('remboursable')!=null)
            {
                $condition=0;
            }
            if($this->input->get('modifiable')==null && $this->input->get('remboursable')==null)
            {
                $condition=5;
            }
            $prixReduit=$tarif*($reduction/100);
            $prixReduitTotal=$tarif-$prixReduit;
            $montant=$prixReduitTotal*($prix/100);
            // echo "Tarif: ".$tarif." reduction ".$reduction.", prix reduit ". $prixReduit;
            // echo " total: ".$prixReduitTotal;
            // echo "prix: ".$prix." montant est: ".$montant; 
            $data["IDUTILISATEUR"]=$utilisateur;
            $data["IDVOL"]=$vol;
            $data["OPTIONS"]=$option;
            $data["CONDITIONS"]=$condition;
            // var_dump($condition);
            $data["MONTANT"]=$montant;
            $data["DATY"]=Date('Y-m-d');
            $data["ETAT"]=0;
            $data["NOM"]=$nom;
            $data["PRENOM"]=$prenom;
            $data["MODIF"]=0;
            $data["REMBOURSER"]=0;
            // var_dump($data);
            $this->UtilisateurModel->acheter($data);
            //ETO NO MANAO PDF
            $listAvion=$this->VolModel->listVolsById($vol);
            // var_dump($listAvion);
            $datas["numero"]=$listAvion[0]->NUMERO;
            $datas["depart"]=$listAvion[0]->DEPART;
            $datas["arrive"]=$listAvion[0]->ARRIVE;
            $datas["avion"]=$listAvion[0]->REFERENCE;
            $datas["daty"]=$listAvion[0]->DATY;
            $datas["heure"]=$listAvion[0]->HEURE;
            $cate="Adulte";
            if($option==1)
            {
                $cate="Enfant";
            }
            else if($option==2)
            {
                $cate="Bebe";
            }
            $datas["option"]=$cate;
            $cond="Non remboursable et non modifiable";
            if($condition==1)
            {
                $cond="Modifiable";
            }
            else if($condition==2)
            {
                $cond="Remboursable";
            }
            else if($condition==0)
            {
                $cond="Modifiable et remboursable";
            }
            $datas["condition"]=$cond;
            $datas["prix"]=$montant;
            $datas["nom"]=$nom;
            $datas["prenom"]=$prenom;
            $this->load->view('pdf', $datas);
            // redirect('UtilisateurController/accueil');
        }
        catch(Exception $e)
        {
            $datas['error']=$e->getMessage();
            $data["achats"]=$this->VolModel->listAchat($this->session->userdata('Utilisateur')->IDUTILISATEUR);
            $data["vol"]=$this->VolModel->listVols();
            $this->load->view("accueil",$datas);
        }
    }
    public function accueil()
    {
        $data["error"]="";
        // var_dump($this->session->userdata('Utilisateur')->IDUTILISATEUR);
        $data["achats"]=$this->VolModel->listAchat($this->session->userdata('Utilisateur')->IDUTILISATEUR);
        $data["achat"]=$this->VolModel->listAchatModifiable($this->session->userdata('Utilisateur')->IDUTILISATEUR);
        $data["vol"]=$this->VolModel->listVols();
        $this->load->view("accueil",$data);
    }
    public function modifierAchat()
    {
        $idVol=$this->input->get('idVol');
        $idAchat=$this->input->get('idAchat');
        // echo $idVol." sy ".$idAchat;
        $this->VolModel->updateAchat($idVol,$idAchat);
        redirect('UtilisateurController/accueil');
    }
    public function modifierVol()
    {
        $data['error']="";
        $idVol=$this->input->get('idVol');
        $idAchat=$this->input->get('idAchat');
        // echo $idAchat;
        $idTrajet=$this->input->get('idTrajet');
        $condition=$this->input->get('condition');
        try{
            if($condition==1 || $condition==0)
            {
                $data['idAchat']=$idAchat;
                $data['vols']=$this->VolModel->propositionVol($idTrajet, $idVol);
                $this->load->view('choixTrajet',$data);
            }
            else 
            {
                throw new Exception("Vous ne pouvez pas modifier votre vol");
            }            
        }
        catch(Exception $e)
        {
            $data['error']=$e->getMessage();
            $data['achats']=$this->VolModel->listAchat($this->session->userdata('Utilisateur')->IDUTILISATEUR);
            $data['achat']=$this->VolModel->listAchatModifiable($this->session->userdata('Utilisateur')->IDUTILISATEUR);
            $data["vol"]=$this->VolModel->listVols();
            $this->load->view('accueil',$data);
            // redirect('UtilisateurController/accueil');
        }
    }
    public function rembourserVol()
    {
        $data['error']="";
        $rembourser=$this->input->get('rembourser');
        $idVol=$this->input->get('idVol');
        $idAchat=$this->input->get('idAchat');
        $montant=$this->input->get('montant');
        $montants=($montant*10)/100;
        $total=$montant-$montants;
        // echo $idAchat;
        $idTrajet=$this->input->get('idTrajet');
        $condition=$this->input->get('condition');
        try{
            if($condition==2 || $condition==0)
            {
                $vo=$this->VolModel->listVolById($idVol);
                $data['idAchat']=$idAchat;
                $data['montants']=$montant;
                $data['montant']=$total;
                $data['numero']=$vo[0]->NUMERO;
                $zao=Date('Y-m-d');
                $heure=Date('H:i');
                if($rembourser==10)
                {
                    throw new Exception("On vous a été remboursé");
                }
                if($vo[0]->DATY<$zao)
                {
                    throw new Exception("Le vol a déja décolé donc on ne peut pas rembourser");
                }
                else if($vo[0]->HEURE==$heure){                    
                    throw new Exception("Le vol a déja décolé donc on ne peut pas rembourser");
                }
                if($condition==0)
                {
                    //MANAO UPDATE NY CHIFFRE D'AFFAIRE
                    $voll=$this->VolModel->getChiffres($idVol);
                    $argent=$voll[0]->MONTANT;
                    $nouveau=$argent-$total;
                    $this->VolModel->updateChiffre($idVol,$nouveau);
                }
                //MODIFIER ANLE ETAT ACHAT ATAO 10
                $this->VolModel->updateRembourser($idAchat);
                $this->load->view('rembourser',$data);
            }
            else 
            {
                throw new Exception("Le montant ne peut pas être remboursé");
            }            
        }
        catch(Exception $e)
        {
            $data['error']=$e->getMessage();
            $data['achats']=$this->VolModel->listAchat($this->session->userdata('Utilisateur')->IDUTILISATEUR);
            $data['achat']=$this->VolModel->listAchatModifiable($this->session->userdata('Utilisateur')->IDUTILISATEUR);
            $data["vol"]=$this->VolModel->listVols();
            $this->load->view('accueil',$data);
            // redirect('UtilisateurController/accueil');
        }
    }
    public function index()
    {
        $data["error"]="";
        $this->load->view("login",$data);
    }
    
    public function connexion()
    {
        try 
        {
            $email = $this->input->post("email");
            $pwd = $this->input->post("pwd");
            $utilisateur = $this->UtilisateurModel->login($email,$pwd);
            if(count($utilisateur)==0)
            {
                throw new Exception("Votre email ou votre mot de passe est incorrect");
            }
            else 
            {
                if($utilisateur[0]->ROLE=="ADMIN")
                {
                    redirect('TrajetController/');
                }   
                else {
                    // var_dump($utilisateur[0]);
                    // var_dump($utilisateur[0]);       
                    $this->session->set_userdata('Utilisateur', $utilisateur[0]);
                    redirect('UtilisateurController/accueil');              
                }
            }
        } 
        catch (Exception $e) 
        {
            $data['error'] = $e->getMessage();
            $this->load->view('login',$data);
        }
    }

    public function deconnexion()
    {
        $this->session->session_destroy;
        redirect('UtilisateurController/');
    }

    public function inscrire()
    {
        $data['error'] = "";
        $this->load->view('inscription', $data);
    }
    public function inscription()
    {
        try {
            $config['upload_path'] = './assets/img/pdp';
            $config['allowed_types'] = 'jpg|png';
            $config['max_size'] = 10000;
            $config['max_width'] = 5000;
            $config['max_height'] = 5000;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('photo')) {
                $photo = "default.jpg";
            } else {
                $fileData = array('upload_data' => $this->upload->data());
                $photo = $fileData['upload_data']['file_name'];
            }
            $data = array();
            $data["NOM"] = $this->input->post('nom');
            $data["PRENOM"] = $this->input->post('prenom');
            $data["EMAIL"] = $this->input->post('email');
            if(strpos($data["NOM"],"<") || strpos($data["PRENOM"],"<") || strpos($data["EMAIL"],"<"))
            {
                throw new Exception ("Votre nom ou prénom ou email ne doit pas contenir des caractères spéciaux");
            }
            $data["PASSWORD"] = $this->input->post('pwd');
            $user=$this->UtilisateurModel->verify($data["EMAIL"]);
            if(count($user)!=0)
            {
                throw new Exception ("Il y a deja un autre utilisateur qui utilise cette e-mail");
            }
            $data["ROLE"] = "CLIENT";
            $data["PHOTO"] = $photo;
            // var_dump($data);
            $this->UtilisateurModel->inscription($data);
            redirect("/UtilisateurController/index");
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
            $this->load->view('inscription', $data);
        }
    }
}