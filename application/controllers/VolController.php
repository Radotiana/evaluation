<?php
defined('BASEPATH') or exit('No direct script access allowed');
class VolController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("VolModel");
        $this->load->model("TrajetModel");
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $data['error']="";
        $data['vol']=$this->VolModel->listVols();
        $this->load->view('listVol', $data);
    }
    public function modifierVol()
    {
        $data['error']="";
        $idVol=$this->input->get('idVol');
        $vols=$this->VolModel->listVolById($idVol);
        $data['vol']=$vols;
        $data['trajet']=$this->TrajetModel->listTrajets();
        $data['avion']=$this->VolModel->listAvion();
        
        $this->load->view('updateVol',$data);
    }
    public function supprimerVol()
    {
        $data['error']="";
        $idVol=$this->input->get('idVol');
        $this->VolModel->deleteVol($idVol);
        redirect('VolController/');
    }
    public function updateVol()
    {
        $idVol=$this->input->get('idVol');
        $data["error"]="";
        try {
            $numero=$this->input->get('numero');
            $trajet=$this->input->get('trajet');
            $avion=$this->input->get('avion');
            $daty=$this->input->get('daty');
            $heure=$this->input->get('heure');
            $datas["NUMERO"]=$numero;
            $datas["IDTRAJET"]=$trajet;
            $datas["IDAVION"]=$avion;
            $datas["DATY"]=$daty;
            $datas["HEURE"]=$heure;
            $date=Date('Y-m-d');
            $heures=Date('H:i');
            /*$vols=$this->VolModel->listVolCondition($trajet,$avion,$daty,$heure);
            if(count($vols)!=0)
            {
                throw new Exception("Il y a deja une vol qui correspond à ce moment avec les même trajet et avion");
            }*/
            if($daty<$date)
            {
                throw new Exception("La date du vol ne doit pas être inférieure au date du jour");
            }
            elseif ($daty==$date) 
            {
                if($heure<=$heures)
                {
                    throw new Exception("L'heure du vol ne doit pas être inférieure à l'heure de la saisie");
                }
            }
            $this->VolModel->updateVol($idVol, $datas);
            redirect('VolController/');   
        } catch (Exception $e) {
            $data['error']=$e->getMessage();
            $data['vol']=$this->VolModel->listVols();
            $this->load->view('listVol', $data);
        }
    }
    public function ajoutVol()
    {
        $data['error']="";
        $data['trajet']=$this->TrajetModel->listTrajets();
        $data['avion']=$this->VolModel->listAvion();
        $this->load->view('addVol', $data);
    }
    public function addVol()
    {
        $data["error"]="";
        try {
                $numero=$this->input->get('numero');
                if(preg_match("^[a-zA-Z]{2}+[0-9]{2}$^", $numero))
                {
                    $trajet=$this->input->get('trajet');
                    $avion=$this->input->get('avion');
                    $daty=$this->input->get('daty');
                    $heure=$this->input->get('heure');
                    $datas["NUMERO"]=$numero;
                    $datas["IDTRAJET"]=$trajet;
                    $datas["IDAVION"]=$avion;
                    $datas["DATY"]=$daty;
                    $datas["HEURE"]=$heure;
                    $date=Date('Y-m-d');
                    $heures=Date('H:i');
                    $datas["ETAT"]=0;
                    $vols=$this->VolModel->listVolCondition($trajet,$avion,$daty,$heure);
                    if(count($vols)!=0)
                    {
                        throw new Exception("Il y a deja une vol qui correspond à ce moment avec les même trajet et avion");
                    }
                    if($daty<$date)
                    {
                        throw new Exception("La date du vol ne doit pas être inférieure au date du jour");
                    }
                    elseif ($daty==$date) 
                    {
                        if($heure<=$heures)
                        {
                            throw new Exception("L'heure du vol ne doit pas être inférieure à l'heure de la saisie");
                        }
                    }
                    
                    // echo $date." - ".$heure;
                    $this->VolModel->addVol($datas);
                    redirect('VolController/'); 
                }
                else 
                {
                    $data["error"]="Le numero du vol doit comporter 2 lettres successives suivis de 2 chiffres successives";
                    $data['trajet']=$this->TrajetModel->listTrajets();
                    $data['avion']=$this->VolModel->listAvion();
                    $this->load->view('addVol', $data);
                }
            
        } catch (Exception $e) {
            $data['error']=$e->getMessage();
            $data['trajet']=$this->TrajetModel->listTrajets();
            $data['avion']=$this->VolModel->listAvion();
            $this->load->view('addVol', $data);
        }
    }
    public function search()
    {
        $mot=$this->input->get('search');
        $data['error']="";
        $data['vol']=$this->VolModel->search($mot);
        $this->load->view('listVol', $data);
    }
}