<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TarifModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function countTarif()
    {
        $query = $this->db->query("Select count(*) as NOMBRE from tarifs");
        return $query->result();
    }
    public function listTarifs()
    {
        if(isset($_GET['order_by']))
        {
            $this->db->order_by($_GET['order_by'], 'ASC');
        }
        return $this->db->get('TARIFS')->result();   
    }
    public function listTarif($debuts)
    {
        $debut = (($debuts - 1) * 7)+7;
        $query = $this->db->query("Select * from tarifs  order by prix asc limit " . $debut . ",7");
        return $query->result();
    }
    public function listTarifById($id)
    {
        $this->db->where('IDTARIF', $id);
        return $this->db->get('TARIF')->result();
    }
    public function listTarifByIdTrajet($idTrajet)
    {
        $this->db->where('IDTRAJET', $idTrajet);
        return $this->db->get('TARIF')->result();
    }
    public function addTarif($data)
    {
        $this->db->insert('TARIF', $data);
    }
    public function updateTarif($idTarif, $data)
    {
        $this->db->where('IDTARIF', $idTarif);
        $this->db->update('TARIF', $data);
    }
    public function deleteTarif($idTarif)
    {
        $this->db->delete('TARIF', 'IDTARIF=' . $idTarif);
    }
    public function search($mot)
    {
        $query = $this->db->query("SELECT DISTINCT(TARIFS.IDTARIF),TARIFS.IDTRAJET,TARIFS.DEPART,TARIFS.ARRIVE,TARIFS.PRIX FROM TARIFS JOIN TRAJET ON TARIFS.IDTRAJET=TRAJET.IDTRAJET JOIN VILLE ON TRAJET.DEPART=VILLE.IDVILLE OR TRAJET.ARRIVE=VILLE.IDVILLE WHERE TARIFS.PRIX LIKE '%".$mot."%' OR TRAJET.DISTANCE LIKE '%".$mot."%' OR VILLE.LIBELE LIKE '%".$mot."%'");
        return $query->result();
    }
}