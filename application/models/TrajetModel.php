<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TrajetModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function getVille()
    {
        return $this->db->get('VILLE')->result();
    }
    public function countTrajet()
    {
        $query = $this->db->query("Select count(*) as NOMBRE from trajets");
        return $query->result();
    }
    public function listTrajet($debuts)
    {
        $debut = (($debuts - 1) * 7)+7;
        $query = $this->db->query("Select * from trajets  order by distance asc limit " . $debut . ",7");
        return $query->result();
    }
    public function listTrajets()
    {
        if(isset($_GET['order_by']))
        {
            $this->db->order_by($_GET['order_by'], 'ASC');
        }
        return $this->db->get('TRAJETS')->result();
    }
    public function listTrajetById($id)
    {
        $this->db->where('IDTRAJET', $id);
        return $this->db->get('TRAJET')->result();
    }
    public function listTrajetDepartArrive($depart,$arrive)
    {
        $this->db->where('DEPART', $depart);
        $this->db->where('ARRIVE', $arrive);
        return $this->db->get('TRAJET')->result();
    }
    public function addTrajet($data)
    {
        $this->db->insert('TRAJET', $data);
    }
    public function updateTrajet($idTrajet, $data)
    {
        $this->db->where('IDTRAJET', $idTrajet);
        $this->db->update('TRAJET', $data);
    }
    public function deleteTrajet($idTrajet)
    {
        $this->db->delete('TRAJET', 'IDTRAJET=' . $idTrajet);
    }
    public function search($mot)
    {
        $query = $this->db->query("SELECT * FROM TRAJETS WHERE DEPART LIKE '%".$mot."%' OR ARRIVE LIKE '%".$mot."%' OR DISTANCE LIKE '%".$mot."%'");
        return $query->result();
    }
}