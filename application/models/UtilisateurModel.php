<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UtilisateurModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function verify($email)
    {
        $this->db->where('EMAIL', $email);
        return $this->db->get('UTILISATEUR')->result();
    }
    public function login($email, $pwd)
    {
        $this->db->where('EMAIL', $email);
        $this->db->where('PASSWORD', hash('sha1', $pwd));
        return $this->db->get('UTILISATEUR')->result();
    }
    public function inscription($data)
    {
        $data["PASSWORD"] = hash("sha1", $data["PASSWORD"]);
        $this->db->insert('UTILISATEUR', $data);
    }
    public function acheter($data)
    {
        $this->db->insert('ACHAT', $data);
    }
    public function listVolPrevisionnelle($idVol)
    {
        $query = $this->db->query("SELECT ACHATS.*, UTILISATEUR.NOM AS NOMS,UTILISATEUR.PRENOM AS PRENOMS, AVION.REFERENCE,TRAJETS.DEPART,TRAJETS.ARRIVE FROM ACHATS JOIN UTILISATEUR ON UTILISATEUR.IDUTILISATEUR=ACHATS.IDUTILISATEUR JOIN VOL ON ACHATS.IDVOL=VOL.IDVOL JOIN AVION ON AVION.IDAVION=VOL.IDAVION JOIN TRAJETS ON ACHATS.IDTRAJET=TRAJETS.IDTRAJET WHERE VOL.ETAT=0 AND ACHATS.ETAT=0 AND ACHATS.IDVOL=".$idVol);
        return $query->result();
    }

    public function updateEtatAchat($idAchat)
    {
        $this->db->set('ETAT', 10);
        $this->db->where('IDACHAT', $idAchat);
        $this->db->update('ACHAT'); 
    }
    public function updateVol($idVol)
    {
        $this->db->set('ETAT', 10);
        $this->db->where('IDVOL', $idVol);
        $this->db->update('VOL'); 
    }
}