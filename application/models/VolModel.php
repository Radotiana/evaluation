<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VolModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function countVol()
    {
        $query = $this->db->query("Select count(*) as NOMBRE from vols");
        return $query->result();
    }
    public function listVol($debuts)
    {
        $debut = (($debuts - 1) * 7)+7;
        $query = $this->db->query("Select * from vols order by daty asc limit " . $debut . ",7");
        return $query->result();
    }
    public function listVols()
    {
        if(isset($_GET['order_by']))
        {
            $this->db->order_by($_GET['order_by'], 'ASC');
        }
        $this->db->where('ETAT', 0);
        return $this->db->get('VOLS')->result();
    }
    public function listTousVol()
    {
        if(isset($_GET['order_by']))
        {
            $this->db->order_by($_GET['order_by'], 'ASC');
        }
        return $this->db->get('VOLS')->result();
    }
    public function listVolParti()
    {
        if(isset($_GET['order_by']))
        {
            $this->db->order_by($_GET['order_by'], 'ASC');
        }
        $this->db->where('ETAT', 10);
        return $this->db->get('VOLS')->result();
    }
    public function listVolCondition($idTrajet, $idVol, $daty, $heure)
    {
        $query = $this->db->query("SELECT * FROM VOL WHERE IDTRAJET=".$idTrajet." AND IDVOL=".$idVol." AND DATY='".$daty."' AND HEURE='".$heure."'");
        return $query->result();
    }
    public function listAvion()
    {
        return $this->db->get('AVION')->result();
    }
    public function listVolById($id)
    {
        $this->db->where('IDVOL', $id);
        return $this->db->get('VOL')->result();
    }
    public function listVolsById($id)
    {
        $this->db->where('IDVOL', $id);
        return $this->db->get('VOLS')->result();
    }
    public function addVol($data)
    {
        $this->db->insert('VOL', $data);
    }
    public function updateVol($idVol, $data)
    {
        $this->db->where('IDVOL', $idVol);
        $this->db->update('VOL', $data);
    }
    public function deleteVol($idVol)
    {
        $this->db->delete('VOL', 'IDVOL=' . $idVol);
    }
    public function search($mot)
    {
        $query = $this->db->query("SELECT DISTINCT(VOLS.IDVOL),VOLS.NUMERO,VOLS.DEPART,VOLS.ARRIVE,VOLS.REFERENCE, VOLS.DATY,VOLS.HEURE FROM VOLS JOIN AVION ON AVION.IDAVION=VOLS.IDAVION JOIN TRAJET ON VOLS.IDTRAJET=TRAJET.IDTRAJET JOIN VILLE ON TRAJET.DEPART=VILLE.IDVILLE OR TRAJET.ARRIVE=VILLE.IDVILLE WHERE VOLS.NUMERO LIKE '%".$mot."%' OR VOLS.DATY LIKE '%".$mot."%' OR VOLS.HEURE LIKE '%".$mot."%' OR AVION.REFERENCE LIKE '%".$mot."%' OR AVION.NBPLACE LIKE '%".$mot."%' OR TRAJET.DISTANCE LIKE '%".$mot."%' OR VILLE.LIBELE LIKE '%".$mot."%'");
        return $query->result();
    }
    public function getTarifByVol($idVol)
    {
        $query = $this->db->query("Select TARIF.PRIX FROM TARIF JOIN VOL ON VOL.IDTRAJET=TARIF.IDTRAJET WHERE VOL.IDVOL=".$idVol);
        return $query->result();
    }
    public function getAvionByVol($idVol)
    {
        $query = $this->db->query("Select AVION.NBPLACE FROM AVION JOIN VOL ON VOL.IDAVION=AVION.IDAVION WHERE VOL.IDVOL=".$idVol);
        return $query->result();
    }
    public function getAchatTotal($idVol)
    {
        $query = $this->db->query("Select count(*) AS NB from achat where idVol=".$idVol);
        return $query->result();
    }
    public function listAchat($idUtilisateur)
    {
        $this->db->where('IDUTILISATEUR', $idUtilisateur);
        $this->db->where('ETATS',0);
        $this->db->where('ETAT',0);
        return $this->db->get('ACHATS')->result();
    }
    public function listAchatModifiable($idUtilisateur)
    {
        $this->db->where('IDUTILISATEUR', $idUtilisateur);
        $this->db->where('MODIF',10);
        // $this->db->where('ETAT',0);
        return $this->db->get('ACHATS')->result();
    }
    public function propositionVol($idTrajet, $idVol)
    {
        $query = $this->db->query("SELECT *  FROM VOLS where ETAT=0 AND idtrajet=".$idTrajet." and idvol!=".$idVol);
        return $query->result();
    }
    public function updateAchat($idVol, $idAchat)
    {
        $this->db->set('IDVOL', $idVol);
        $this->db->set('MODIF', 0);
        $this->db->where('IDACHAT', $idAchat);
        $this->db->update('ACHAT'); 
    }
    public function updateModif($idAchat)
    {
        $this->db->set('MODIF', 10);
        $this->db->where('IDACHAT', $idAchat);
        $this->db->update('ACHAT'); 
    }
    public function getChiffre($idVol)
    {
        $query = $this->db->query("SELECT Achat.*, Vol.etat AS ETATS FROM achat JOIN vol ON achat.idVol=Vol.idVol WHERE Achat.idVol=".$idVol);
        return $query->result();
    }
    public function addChiffre($data)
    {
        $this->db->insert('CHIFFRE', $data);
    }
    public function getChiffres($idVol)
    {
       $this->db->where('IDVOL',$idVol);
       return $this->db->get('CHIFFRE')->result();
    }
    public function updateChiffre($idVol,$chiffre)
    {
        $this->db->set('MONTANT', $chiffre);
        $this->db->where('IDVOL', $idVol);
        $this->db->update('CHIFFRE'); 
    }
    public function updateRembourser($idAchat)
    {
        $this->db->set('REMBOURSER', 10);
        $this->db->where('IDACHAT', $idAchat);
        $this->db->update('ACHAT'); 
    }
}