<?php
    if ($error != null) { ?>
    <script>
        alert("<?php echo $error; ?>");
    </script>
<?php } ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Acheter un billet</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/normalize.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/style.css">

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body{
            background-color: #abb6c2;
        }
        a{
            color: white; 
            text-decoration:none; 
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Evaluation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/accueil');?>">Accueil<span class="sr-only">(current)</span></a>
                </li> 
            </ul>
            <a class="nav-link" href="<?php echo base_url('UtilisateurController/deconnexion'); ?>"><button class="btn btn-danger">Se deconnecter</button></a>
        </div>
    </nav>
    
    <div class="container">
        <h1>Acheter un billet</h1>
        <br>
        <form method="get" action="<?php echo base_url('UtilisateurController/acheter'); ?>">
            <label>Vol</label>
            <select name="vol">
                <?php for($i=0; $i<count($vol); $i++) { ?>
                    <option value="<?php echo $vol[$i]->IDVOL; ?>"><?php echo $vol[$i]->NUMERO; ?></option>
                <?php } ?>
            </select>
            <br>
            <label>Nom</label>
            <input type="text" name="nom">
            <br>
            <label>Prénom</label>
            <input type="text" name="prenom">
            <br>
            <label>Options</label>
            <select name="option">
                <option value="0">Adulte</option>
                <option value="1">Enfant</option>
                <option value="2">Bebe</option>
            </select>
            <br>
            <label>Conditions</label>
            <br>
                <input type="checkbox" value="0" name="modifiable" /> Modifiable
                <input type="checkbox" value="1" name="remboursable" /> Remboursable
            <br><br>
            <button class="btn btn-success">Acheter</button>
        </form>
        <br>
    <h1>Liste de mes achats</h1>
    <div style="text-align:center" class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Numero du vol</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Heure</th>
                                            <th data-tri="1" class="selection" data-type="num" scope="col">Prix (en Ariary)</th>
                                            <th scope="col">Modifier</th>
                                            <th scope="col">Rembourser</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php for($i=0; $i<count($achats); $i++) { ?>
                                        <tr class="table-active">
                                            <td><?php echo $achats[$i]->NUMERO; ?></td>
                                            <td><?php echo $achats[$i]->DATE; ?></td>
                                            <td><?php echo $achats[$i]->HEURE; ?></td>
                                            <td style="text-align: right"><?php echo number_format($achats[$i]->MONTANT); ?></td>
                                            <td><a href="<?php echo base_url('UtilisateurController/modifierVol?idVol='.$achats[$i]->IDVOL.'&condition='.$achats[$i]->CONDITIONS.'&idTrajet='.$achats[$i]->IDTRAJET.'&idAchat='.$achats[$i]->IDACHAT); ?>"><button class="btn btn-warning">Modifier</button></a></td>
                                            <td><a href="<?php echo base_url('UtilisateurController/rembourserVol?rembourser='.$achats[$i]->REMBOURSER.'&montant='.$achats[$i]->MONTANT.'&idVol='.$achats[$i]->IDVOL.'&condition='.$achats[$i]->CONDITIONS.'&idTrajet='.$achats[$i]->IDTRAJET.'&idAchat='.$achats[$i]->IDACHAT); ?>"><button class="btn btn-danger">Rembourser</button></a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
                
            </div><!-- .animated -->
        </div><!-- .content -->
        <br>
        <h1>Liste de mes achats modifiable</h1>
        <div style="text-align:center" class="content">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Data Table</strong>
                                </div>
                                <div class="card-body">
                                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">Numero du vol</th>
                                                <th scope="col">Date</th>
                                                <th scope="col">Heure</th>
                                                <th data-tri="1" class="selection" data-type="num" scope="col">Prix (en Ariary)</th>
                                                <th scope="col">Modifier</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i=0; $i<count($achat); $i++) { ?>
                                            <tr class="table-active">
                                                <td><?php echo $achat[$i]->NUMERO; ?></td>
                                                <td><?php echo $achat[$i]->DATE; ?></td>
                                                <td><?php echo $achat[$i]->HEURE; ?></td>
                                                <td style="text-align: right"><?php echo number_format($achat[$i]->MONTANT); ?></td>
                                                <td><a href="<?php echo base_url('UtilisateurController/modifierVol?idVol='.$achat[$i]->IDVOL.'&condition='.$achat[$i]->CONDITIONS.'&idTrajet='.$achat[$i]->IDTRAJET.'&idAchat='.$achat[$i]->IDACHAT); ?>"><button class="btn btn-warning">Modifier</button></a></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!-- .animated -->
            </div><!-- .content -->
        </div>
    </div>
    
    <script src="<?php echo base_url(); ?>assets/table/js/main.js"></script>


    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/init/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
  </script>
</body>
</html>