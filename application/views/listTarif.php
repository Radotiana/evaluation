<?php
    if ($error != null) { ?>
    <script>
        alert("<?php echo $error; ?>");
    </script>
<?php } ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des tarif</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Liste des tarifs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/normalize.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/style.css">

    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'> -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <style>
        body{
            background-color: #abb6c2;
        }
        a{
            color: white; 
            text-decoration:none; 
        }

        /* Classe obligatoire pour les flèches */
        .flecheDesc {
          width: 0; 
          height: 0; 
          float:right;
          margin: 10px;
          border-left: 5px solid transparent;
          border-right: 5px solid transparent;
          border-bottom: 5px solid black;
        }
        .flecheAsc {
          width: 0; 
          height: 0;
          float:right;
          margin: 10px;
          border-left: 5px solid transparent;
          border-right: 5px solid transparent;
          border-top: 5px solid black;
        }
        
        /* Classe optionnelle pour le style */
        .tableau {width:100%;table-layout: fixed;border-collapse: collapse;}
        .tableau td {padding:.3rem}
        .zebre tbody tr:nth-child(odd) {background-color: #d6d3ce;border-bottom:1px solid #ccc;color:#444;}
        .zebre tbody tr:nth-child(even) {background-color: #c6c3bd;border-bottom:1px solid #ccc;color:#444;}
        .zebre tbody tr:hover:nth-child(odd) {background-color: #999690;color:#ffffff;}
        .zebre tbody tr:hover:nth-child(even) {background-color: #999690;color:#ffffff;}
        .avectri th {text-align:center;padding:5px 0 0 5px;vertical-align: middle;background-color:#999690;color:#444;cursor:pointer;
            -webkit-touch-callout: none;
          -webkit-user-select: none;
          -khtml-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          -o-user-select: none;
          user-select: none;
        }
        .avectri th.selection {background-color:#5d625c;color:#fff;}
        .avectri th.selection .flecheDesc {border-bottom-color: white;}
        .avectri th.selection .flecheAsc {border-top-color: white;}
        .zebre tbody td:nth-child(3) {text-align:center;}
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Evaluation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('TrajetController?nb=0/');?>">Trajet<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('VolController?nb=0/');?>">Vol<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tarif<span class="sr-only">(current)</span></a>
                </li>  
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/listPrevision');?>">Prevision<span class="sr-only">(current)</span></a>
                </li>      
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/chiffre');?>">Chiffre<span class="sr-only">(current)</span></a>
                </li>          
            </ul>
            <a class="nav-link" href="<?php echo base_url('UtilisateurController/deconnexion'); ?>"><button class="btn btn-danger">Se deconnecter</button></a>

        </div>
    </nav>
    
    <div class="container" style="text-align: center">
        <h1>Liste des tarifs</h1>
        <br>
        <form method="get" action="<?php echo base_url('TarifController/search');?>">
            <input type="text" name="search" placeholder="Rechercher">
            <button class="btn btn-secondary">Rechercher</button>
        </form>
        <br>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th scope="col"><a style="color:black" href="<?php echo base_url('TarifController?order_by=DEPART'); ?>">Depart</a></th>
                                        <th scope="col"><a style="color:black" href="<?php echo base_url('TarifController?order_by=ARRIVE'); ?>">Arrivée</a></th>
                                        <th data-tri="1" class="selection" data-type="num" scope="col"><a style="color:black" href="<?php echo base_url('TarifController?order_by=PRIX'); ?>">Prix</a></th>
                                        <th scope="col">Modifier</th>
                                        <th scope="col">Supprimer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php for($i=0; $i<count($tarif); $i++) { ?>
                                        <tr class="table-active">
                                            <td><?php echo $tarif[$i]->DEPART; ?></td>
                                            <td><?php echo $tarif[$i]->ARRIVE; ?></td>
                                            <td style="text-align: right"><?php echo number_format($tarif[$i]->PRIX); ?></td>
                                            <td><a href="<?php echo base_url('TarifController/modifierTarif?idTarif='.$tarif[$i]->IDTARIF); ?>"><button class="btn btn-warning">Modifier</button></a></td>
                                            <td><a href="<?php echo base_url('TarifController/supprimerTarif?idTarif='.$tarif[$i]->IDTARIF); ?>"><button class="btn btn-danger">Supprimer</button></a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <!-- <?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>
        <br> -->
        <a href="<?php echo base_url('TarifController/ajoutTarif'); ?>"><button class="btn btn-info">Ajouter un tarif</button></a>
        <br><br><br>       
    </div>
    <script src="<?php echo base_url(); ?>assets/table/js/main.js"></script>


    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/init/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
  </script>
</body>
</html>