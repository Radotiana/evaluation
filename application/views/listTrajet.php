<?php
    if ($error != null) { ?>
    <script>
        alert("<?php echo $error; ?>");
    </script>
<?php } ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des trajets</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/normalize.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/table/css/style.css">

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body{
            background-color: #abb6c2;
        }
        a{
            color: white; 
            text-decoration:none; 
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Evaluation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Trajet<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('VolController?nb=0/');?>">Vol<span class="sr-only">(current)</span></a>
                </li>      
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('TarifController?nb=0/');?>">Tarif<span class="sr-only">(current)</span></a>
                </li>       
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/listPrevision');?>">Prevision<span class="sr-only">(current)</span></a>
                </li>            
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/chiffre');?>">Chiffre<span class="sr-only">(current)</span></a>
                </li>  
            </ul>
            <a class="nav-link" href="<?php echo base_url('UtilisateurController/deconnexion'); ?>"><button class="btn btn-danger">Se deconnecter</button></a>

        </div>
    </nav>
    
    <div class="container" style="text-align: center">
        <h1>Liste des trajets</h1>
        <br>
        <form method="get" action="<?php echo base_url('TrajetController/search');?>">
            <input type="text" name="search" placeholder="Rechercher">
            <button class="btn btn-secondary">Rechercher</button>
        </form>
        <br>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Trajets</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col"><a style="color:black" href="<?php echo base_url('TrajetController?order_by=DEPART'); ?>">Depart</a></th>
                                        <th scope="col"><a style="color:black" href="<?php echo base_url('TrajetController?order_by=ARRIVE'); ?>">Arrivée</a></th>
                                        <th scope="col"><a style="color:black" href="<?php echo base_url('TrajetController?order_by=DISTANCE'); ?>">Distance (en km)</a></th>
                                        <th scope="col">Modifier</th>
                                        <th scope="col">Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for($i=0; $i<count($trajet); $i++) { ?>
                                        <tr class="table-active">
                                            <td><?php echo $trajet[$i]->DEPART; ?></td>
                                            <td><?php echo $trajet[$i]->ARRIVE; ?></td>
                                            <td style="text-align: right"><?php echo $trajet[$i]->DISTANCE; ?></td>
                                            <td><a href="<?php echo base_url('TrajetController/modifierTrajet?idTrajet='.$trajet[$i]->IDTRAJET); ?>"><button class="btn btn-warning">Modifier</button></a></td>
                                            <td><a href="<?php echo base_url('TrajetController/supprimerTrajet?idTrajet='.$trajet[$i]->IDTRAJET); ?>"><button class="btn btn-danger">Supprimer</button></a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <a href="<?php echo base_url('TrajetController/ajoutTrajet'); ?>"><button class="btn btn-info">Ajouter un trajet</button></a>
        <br><br><br>
        
    </div>
    <script src="<?php echo base_url(); ?>assets/table/js/main.js"></script>


    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/table/js/init/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
      } );
  </script>
</body>
</html>