<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Connexion</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/brands.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/solid.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <style>
        /* Required for full background image */

        html,
        body,
        header,
        .view {
            height: 100%;
        }

        @media (max-width: 740px) {

            html,
            body,
            header,
            .view {
                height: 1000px;
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {

            html,
            body,
            header,
            .view {
                height: 650px;
            }
        }

        .top-nav-collapse {
            background-color: #3f51b5 !important;
        }

        .navbar:not(.top-nav-collapse) {
            background: transparent !important;
        }

        @media (max-width: 991px) {
            .navbar:not(.top-nav-collapse) {
                background: #3f51b5 !important;
            }
        }

        .rgba-gradient {
            background: -webkit-linear-gradient(45deg, rgba(0, 0, 0, 0.7), rgba(72, 15, 144, 0.4) 100%);
            background: -webkit-gradient(linear, 45deg, from(rgba(0, 0, 0, 0.7), rgba(72, 15, 144, 0.4) 100%));
            background: linear-gradient(to 45deg, rgba(0, 0, 0, 0.7), rgba(72, 15, 144, 0.4) 100%);
        }

        .card {
            background-color: rgba(126, 123, 215, 0.2);
        }

        .md-form label {
            color: #ffffff;
        }

        h6 {
            line-height: 1.7;
        }
    </style>
</head>

<body>
    <!-- Main navigation -->
    <header>
        <!-- Full Page Intro -->
        <div class="view" style="background-image: url('<?php echo base_url(); ?>assets/img/index2.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
            <!-- Mask & flexbox options-->
            <div class="mask rgba-gradient d-flex justify-content-center align-items-center">
                <!-- Content -->
                <div class="container">
                    <!--Grid row-->
                    <div class="row mt-4">
                        <!--Grid column-->
                        <div class="col-md-6 mb-5 mt-md-0 mt-5 white-text text-center text-md-left">
                            <h1 class="h1-responsive font-weight-bold wow fadeInLeft" data-wow-delay="0.3s" style="text-align: left;">Compagnie aérienne ✈</h1>
                            <hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s">
                            <h6 class="mb-3 wow fadeInLeft" data-wow-delay="0.3s">C'est une application web malgache qui permet d'acheter des billets d'avion en ligne
                                Si vous n'êtes pas encore membre, veuillez nous rejoindre en vous inscrivant 😉
                            </h6>
                            <a href="<?php echo base_url('UtilisateurController/inscrire'); ?>" class="btn btn-outline-white wow fadeInLeft" data-wow-delay="0.3s" style="text-align: center;">Inscription</a>
                        </div>
                        <!-- Grid column -->
                        <!--Grid column-->
                        <div class="col-md-6 col-xl-5 mb-4">
                            <!--Form-->
                            <form action="<?php echo base_url('UtilisateurController/connexion'); ?>" method="post">
                                <div class="card wow fadeInRight" data-wow-delay="0.3s" style="border-radius: 3%;">
                                    <div class="card-body" style="border-radius: 8px">
                                        <!--Header-->
                                        <div class="text-center">
                                            <h3 class="white-text">Connexion</h3>
                                            <hr class="hr-light">
                                        </div>
                                        <!--Body-->
                                        <div class="md-form">
                                            <i class="fas fa-envelope prefix white-text active"></i>
                                            <input type="email" id="form2" name="email" class="white-text form-control">
                                            <label for="form2" class="active">Votre e-mail</label>
                                        </div>
                                        <div class="md-form">
                                            <i class="fas fa-lock prefix white-text active"></i>
                                            <input type="password" id="form4" name="pwd" class="white-text form-control">
                                            <label for="form4">Votre mot de passe</label>
                                        </div>
                                        <?php
                                        // echo  $error;
                                        if ($error != null) { ?>
                                            <script>
                                                alert("<?php echo $error; ?>");
                                            </script>
                                        <?php } ?>

                                        <div class="text-center mt-4">
                                            <button class="btn btn-outline-white">Se connecter</button>
                                            <hr class="hr-light mb-3 mt-4">
                                            <a href="<?php echo base_url('UtilisateurController/forgetPassword'); ?>">Mot de passe oublié</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--/.Form-->
                        </div>
                        <!--Grid column-->
                    </div>
                    <!--Grid row-->
                </div>
                <!-- Content -->
            </div>
            <!-- Mask & flexbox options-->
        </div>
        <!-- Full Page Intro -->
    </header>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <!-- Tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
    <script>
        new WOW().init();
    </script>
</body>

</html>