<?php
    if ($error != null) { ?>
    <script>
        alert("<?php echo $error; ?>");
    </script>
<?php } ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter un trajet</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body{
            background-color: #abb6c2;
        }
        a{
            color: white; 
            text-decoration:none; 
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Evaluation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('TrajetController/');?>">Trajet<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('VolController/');?>">Vol<span class="sr-only">(current)</span></a>
                </li>      
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('TarifController/');?>">Tarif<span class="sr-only">(current)</span></a>
                </li>       
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/listPrevision');?>">Prevision<span class="sr-only">(current)</span></a>
                </li>      
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('UtilisateurController/chiffre');?>">Chiffre<span class="sr-only">(current)</span></a>
                </li>        
            </ul>
            <a class="nav-link" href="<?php echo base_url('UtilisateurController/deconnexion'); ?>"><button class="btn btn-danger">Se deconnecter</button></a>
        </div>
    </nav>
    
    <div class="container">
        <h1>Modifier un trajet</h1>
        <br>
        <form method="get" action="<?php echo base_url('TrajetController/updateTrajet'); ?>">
            <label>Depart</label>
            <select name="depart">
                <?php for($i=0; $i<count($ville); $i++) { ?>
                    <!-- <option selected="selected" value="<?php echo $trajet[0]->DEPART; ?>"></option> -->
                    <option value="<?php echo $ville[$i]->IDVILLE; ?>"><?php echo $ville[$i]->LIBELE; ?></option>
                <?php } ?>
            </select>
            <br>
            <label>Arrive</label>
            <select name="arrive">
                <?php for($i=0; $i<count($ville); $i++) { ?>
                    <option value="<?php echo $ville[$i]->IDVILLE; ?>"><?php echo $ville[$i]->LIBELE; ?></option>
                <?php } ?>
            </select>
            <br>
            <label>Distance</label>
            <input type="text" name="distance" value="<?php echo $trajet[0]->DISTANCE; ?>">
            <input type="hidden" name="idTrajet" value="<?php echo $trajet[0]->IDTRAJET; ?>">
            <br>
            <button class="btn btn-success">Modifier</button>
        </form>
    </div>
</body>
</html>