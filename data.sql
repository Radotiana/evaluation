CREATE TABLE UTILISATEUR(
    IDUTILISATEUR   int not null AUTO_INCREMENT PRIMARY KEY,
    NOM             varchar(25) not null,
    PRENOM          varchar(25) not null,
    EMAIL           varchar(25) not null,
    PASSWORD        varchar(150) not null,
    ROLE            varchar(10) null,
    PHOTO           varchar(75)
);

CREATE TABLE AVION (
    IDAVION         int not null AUTO_INCREMENT PRIMARY KEY,
    REFERENCE       varchar(15) not null,
    NBPLACE         int not null
);

insert into Avion values(1, 'Twin otter', 19);
insert into Avion values(2, 'Beech craft 200', 8);

CREATE TABLE VILLE (
    IDVILLE         int not null AUTO_INCREMENT PRIMARY KEY,
    LIBELE          varchar(20) not null
);

insert into Ville values(1, 'Antananarivo');
insert into Ville values(2, 'Toamasina');
insert into Ville values(3, 'Mahajanga');
insert into Ville values(4, 'Antsiranana');
insert into Ville values(5, 'Toliara');
insert into Ville values(6, 'Fianarantsoa');

CREATE TABLE TRAJET (
    IDTRAJET        int not null AUTO_INCREMENT PRIMARY KEY,
    DEPART          int not null,
    ARRIVE          int not null,
    DISTANCE        decimal not null          
);
alter table TRAJET add constraint FK_REFERENCE_1 foreign key (DEPART)
references VILLE (IDVILLE) on delete restrict on update restrict;
alter table TRAJET add constraint FK_REFERENCE_2 foreign key (ARRIVE)
references VILLE (IDVILLE) on delete restrict on update restrict;


CREATE TABLE VOL (
    IDVOL           int not null AUTO_INCREMENT PRIMARY KEY, 
    NUMERO          varchar(5) not null UNIQUE,
    IDTRAJET        int not null,
    IDAVION         int not null,
    DATY            date not null,
    HEURE           time not null,
    ETAT            integer not null,              
);
alter table VOL add constraint FK_REFERENCE_3 foreign key (IDTRAJET)
references TRAJET (IDTRAJET) on delete restrict on update restrict;

alter table VOL add constraint FK_REFERENCE_4 foreign key (IDAVION)
references AVION (IDAVION) on delete restrict on update restrict;

CREATE TABLE TARIF (
    IDTARIF         int not null AUTO_INCREMENT PRIMARY KEY,
    IDTRAJET        int not null,
    PRIX            decimal not null   
);

alter table TARIF add constraint FK_REFERENCE_5 foreign key (IDTRAJET)
references TRAJET (IDTRAJET) on delete restrict on update restrict;

CREATE TABLE ACHAT (
    IDACHAT         int not null AUTO_INCREMENT PRIMARY KEY,
    IDUTILISATEUR   int not null,
    IDVOL           int not null,
    OPTIONS         int not null,
    CONDITIONS      int not null,
    MONTANT         decimal not null,
    DATY            date not null,
    ETAT            integer not null,
    MODIF            integer not null,  
    REMBOURSER            integer not null  
);
alter table ACHAT add constraint FK_REFERENCE_6 foreign key (IDUTILISATEUR)
references UTILISATEUR (IDUTILISATEUR) on delete restrict on update restrict;

alter table ACHAT add constraint FK_REFERENCE_7 foreign key (IDVOL)
references VOL (IDVOL) on delete restrict on update restrict;

CREATE TABLE CHIFFRE(
    IDVOL           int not null,
    MONTANT         decimal not null
);
alter table CHIFFRE add constraint FK_REFERENCE_8 foreign key (IDVOL)
references VOL (IDVOL) on delete restrict on update restrict;

-- VIEWS
-- LISTE DES TRAJETS
CREATE VIEW TRAJETS AS (Select IDTRAJET, v1.libele as DEPART, v2.libele AS ARRIVE, trajet.DISTANCE from trajet inner join ville v1 on v1.idville=trajet.depart inner join ville v2 on v2.idville=trajet.arrive);

-- LISTE DES VOLS
CREATE VIEW VOLS AS (Select VOL.ETAT,VOL.IDTRAJET,VOL.IDAVION,VOL.IDVOL,VOL.NUMERO, v1.libele as DEPART, v2.libele AS ARRIVE, AVION.REFERENCE, VOL.DATY,VOL.HEURE from vol join trajet on vol.idtrajet=trajet.idTrajet inner join ville v1 on v1.idville=trajet.depart inner join ville v2 on v2.idville=trajet.arrive join avion on avion.idAvion=Vol.idAvion);

-- LISTE DES TARIFS
CREATE VIEW TARIFS AS (Select TARIF.IDTARIF,TARIF.IDTRAJET, v1.libele as DEPART, v2.libele AS ARRIVE, TARIF.PRIX from TARIF JOIN trajet ON TARIF.IDTRAJET=TRAJET.IDTRAJET inner join ville v1 on v1.idville=trajet.depart inner join ville v2 on v2.idville=trajet.arrive)

-- MAKA PRIX ANA VOL
Select Tarif.PRIX from TARIF JOIN VOL ON VOL.IDTRAJET=TARIF.IDTRAJET WHERE VOL.IDTRAJET=1

-- MAKA ANLE ACHAT
CREATE VIEW ACHATS AS (Select ACHAT.*, VOL.ETAT AS ETATS, VOL.NUMERO,VOL.IDTRAJET,VOL.DATY AS DATE,VOL.HEURE FROM ACHAT JOIN VOL ON ACHAT.IDVOL=VOL.IDVOL);
 WHERE ACHATS.IDUTILISATEUR=3;

-- MAKA ACHATS ANY AM LISTE PREVISIONNELLE
SELECT ACHATS.*, UTILISATEUR.NOM,UTILISATEUR.PRENOM, AVION.REFERENCE,TRAJET.DEPART,TRAJET.ARRIVE FROM ACHATS JOIN UTILISATEUR ON UTILISATEUR.IDUTILISATEUR=ACHATS.IDUTILISATEUR JOIN VOL ON ACHATS.IDVOL=VOL.IDVOL JOIN AVION ON AVION.IDAVION=VOL.IDAVION JOIN TRAJET ON ACHATS.IDTRAJET=TRAJET.IDTRAJET WHERE ACHATS.IDVOL=1;

-- CHIFFRE D'AFFAIRE
SELECT Achat.*, Vol.etat AS ETATS FROM achat JOIN vol ON achat.idVol=Vol.idVol;